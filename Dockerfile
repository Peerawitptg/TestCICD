FROM nginx:latest
ENV http_proxy http://192.168.1.3:3128
ENV https_proxy http://192.168.1.3:3128
ARG HTTP_PROXY http://192.168.1.3:3128
ARG HTTPS_PROXY http://192.168.1.3:3128
WORKDIR /usr/share/nginx/html
COPY . /usr/share/nginx/html
COPY index.html .